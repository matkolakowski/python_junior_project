pandas==1.4.4
numpy==1.23.2
sklearn==0.0
matplotlib==3.5.3
matplotlib-inline==0.1.6
matplotlib-venn==0.11.7
UpSetPlot==0.6.1
pyarrow==9.0.0

