import pathlib
from setuptools import setup, find_packages


HERE = pathlib.Path(__file__).parent
README = (HERE / "README.md").read_text()


def read_requirements():
    """Parses requirements from requirements.txt"""
    reqs_path = HERE / "requirements.txt"
    with open(reqs_path, encoding="utf8") as f:
        reqs = [line.strip() for line in f if not line.strip().startswith("#")]
    return reqs


setup(
    name="python_junior_project",
    version="0.1.0",
    description="A repository containing my projects including machine learning with sklearn library, data "
                "visualization and image processing",
    long_description=README,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/matkolakowski/python_junior_project",
    author="Mateusz Kołakowski",
    python_requires=">=3.8.0",
    # classifiers=[
    #     "Programming Language :: Python :: 3",
    #     "Programming Language :: Python :: 3.8",
    #     "Topic :: Scientific/Engineering",
    # ],
    packages=find_packages(exclude=["tests", "tests.*"]),
    package_data={},
    include_package_data=True,
    install_requires=read_requirements(),
)
