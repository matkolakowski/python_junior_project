# Python Junior Project



## Rationale

The purpose of this repository is to showcase my work and demonstrate my skills and abilities in the field of machine learning, data visualization nad image denoising. Each branch contains a separate project:

- Set-plot
- ML-model-hyperparameter-tune

To learn more about the usage of each project, go to the selected branch and read the tutorial.  I have tried to make the tutorials as clear and concise as possible, but if you have any questions or need further clarification, don't hesitate to reach out to me.

## Prerequisites

Python >= 3.8.0

## Installation
```
git clone https://gitlab.com/matkolakowski/python_junior_project.git
cd python_junior_project
pip install -e .
```

## Example use

Visit individual branches to read the tutorial.

